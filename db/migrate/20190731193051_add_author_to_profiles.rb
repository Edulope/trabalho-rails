class AddAuthorToProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :author, :string
  end
end
